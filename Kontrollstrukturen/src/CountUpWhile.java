import java.util.Scanner;

public class CountUpWhile {
	public static void main(String[] args) { // Mark Kanzler Abgabe 1c)
		int startwert = 1;
		Scanner tastatur = new Scanner (System.in);
		int eingabe = tastatur.nextInt();
		
		while(startwert <= eingabe && eingabe > 0) {
			System.out.println(startwert);
			startwert++;
		}
	}
}
