﻿import java.util.Scanner;

class Fahrkartenautomat { // Mark Kanzlers Abgabe mit whileschleife und Fahrkartenanzahl überprüft

	public static void main(String[] args) {
		char weiter = 'j';
		while (weiter == 'j') {
			Scanner tastatur = new Scanner(System.in);
			double zuZahlenderBetrag;

			zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
			while (zuZahlenderBetrag > 0) {
				zuZahlenderBetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
			}
			fahrkartenAusgeben();
			rueckgeldAusgeben(zuZahlenderBetrag * -1);

			System.out.println("Weiter = j /// Abbruch = Esc");
			weiter = tastatur.next().charAt(0);
		}
	}

	private static double fahrkartenBezahlen(double zuZahlen, Scanner tastatur) {
		System.out.printf("Bitte Zahlen sie: %.2f \n", zuZahlen);
		System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
		zuZahlen -= tastatur.nextDouble();
		System.out.printf("Noch zu zahlen: %.2f \n", zuZahlen);
		return zuZahlen;
	}

	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	private static double fahrkartenbestellungErfassen(Scanner tastatur) {
		System.out.print("Zu zahlender Betrag (EURO): ");
		double betrag = tastatur.nextDouble();
		System.out.print("Anzahl Fahrkarten:");
		int anzahl = tastatur.nextInt();
		if (11 > anzahl && anzahl > 0) {
			return anzahl * betrag;}
		else {
			System.out.println("FEHLER , UNGÜLTIGE TICKETANZAHL, KEINE NEGATIVEN WERTE ODER WERTE ÜBER 10 GÜLTIG");
			anzahl = 1;
			return anzahl * betrag;
		}
		
		
	}

	private static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f \n" + " EURO ", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
}