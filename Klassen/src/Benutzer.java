
public class Benutzer {
	private String name;
	private String passwort;
	private String email;
	private String berechtigungsstatus;
	private String anmeldestatus;
	private String lastLogin;

	public Benutzer(String name, String passwort, String email, String berechtigungsstatus, String anmeldestatus, String lastLogin) {
		this.name = name;
		this.passwort = passwort;
		this.email = email;
		this.berechtigungsstatus = berechtigungsstatus;
		this.anmeldestatus = anmeldestatus;
		this.lastLogin = lastLogin;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPw(String passwort) {
		this.passwort = passwort;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setBstatus(String berechtigungsstatus) {
		this.berechtigungsstatus = berechtigungsstatus;
	}

	public void setAstatus(String anmeldestatus) {
		this.anmeldestatus = anmeldestatus;
	}

	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getName() {
		System.out.println(this.name);
		return this.name;
	}

	public String getPw() {
		System.out.println(this.passwort);
		return this.passwort;
	}

	public String getEmail() {
		System.out.println(this.email);
		return this.email;
	}

	public String getBstatus() {
		System.out.println(this.berechtigungsstatus);
		return this.berechtigungsstatus;
	}

	public String getAstatus() {
		System.out.println(this.anmeldestatus);
		return this.anmeldestatus;
	}

	public String getLastLogin() {
		System.out.println(this.lastLogin);
		return this.lastLogin;
	}
}
